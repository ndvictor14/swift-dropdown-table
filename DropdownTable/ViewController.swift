//
//  ViewController.swift
//  DropdownTable
//
//  Created by Victor Hernandez on 1/27/16.
//  Copyright (c) 2016 Victor Hernandez. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var sports:[String] = [ "Basketball", "Baseball", "Soccer", "Football"]
    var sportSelected:Bool = false
    var selectedSport: Int = 0
    var dropDownViewIsDisplayed: Bool = false

    @IBOutlet weak var dropdownView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dropdownView.delegate = self
        self.dropdownView.dataSource = self
        dropdownView.scrollEnabled = false;

        var height:CGFloat = self.dropdownView.frame.size.height
        var width:CGFloat = self.dropdownView.frame.size.width
        self.dropdownView.frame = CGRectMake(0, -height, width, height)

        self.dropdownView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        if !sportSelected{
            sports.insert("Select a sport...", atIndex: 0)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dropDownViewIsDisplayed{
            return sports.count
        }
        else{
            self.dropdownView.frame.size.height = 50
            return 1
            
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath : indexPath) as! UITableViewCell


        if !dropDownViewIsDisplayed{
            cell.textLabel?.text = sports[ selectedSport ]
        }
        else{
            cell.textLabel?.text = sports[ indexPath.row ]

        }


        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
        self.sports = sortExceptSelected( sports )

        if indexPath.row == 0{
            // if the first row is selected
            if !dropDownViewIsDisplayed{
                var TableHeight = CGFloat( sports.count * 50 )
                self.dropdownView.scrollEnabled = true
                self.dropdownView.frame.size.height = TableHeight
                self.dropdownView.deselectRowAtIndexPath(indexPath, animated: false)
                dropDownViewIsDisplayed = true
                selectedSport = 0
            }
            else{
                self.dropdownView.frame.size.height = 50
                self.dropdownView.scrollEnabled = false
                selectedSport = 0
                dropDownViewIsDisplayed = false
            }

        }
        else{
            self.dropdownView.frame.size.height = 50
            self.dropdownView.scrollEnabled = false
            if !sportSelected{
                sports.removeAtIndex(0)
                sportSelected = true
                selectedSport = indexPath.row - 1
            }
            else{
                selectedSport = indexPath.row
            }
            dropDownViewIsDisplayed = false

        }
            dropdownView.reloadData()
    }
    
    func sortExceptSelected( var someArray: [String] ) -> [String] {
        var sortedArray: [String]
        var first = someArray[ selectedSport ]
        someArray.removeAtIndex(selectedSport)
        sortedArray = sorted( someArray )
        sortedArray.insert(first, atIndex: 0)
        
        return sortedArray
    }

    
    
    // UITableViewDelegate Functions
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }

    
//    func animateDropDownToFrame(frame: CGRect, completion:() -> Void) {
//        if (!self.isAnimating) {
//            self.isAnimating = true
//            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
//                self.dropdownView.frame = frame
//                }, completion: (completed: Bool) -> Void in {
//                  self.isAnimating = false
//                  if (completed) {
//                    completion()
//                  }
//                })
//        }
//    }

}

